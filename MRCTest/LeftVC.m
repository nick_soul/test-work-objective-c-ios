//
//  LeftVC.m
//  MRCTest
//
//  Created by Nikita on 03.06.15.
//  Copyright (c) 2015 com.khodzhaiev. All rights reserved.
//

#import "LeftVC.h"
#import "CoreDataManager.h"
@import CoreData;

@interface LeftVC () <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, assign) IBOutlet UITableView *historyTableView;
@property (nonatomic, retain) NSFetchedResultsController *resultsController;

@end

@implementation LeftVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)dealloc
{
    [super dealloc];
    
    [_resultsController release];
}

- (NSString *)convertDate:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    NSString *result = [formatter stringFromDate:date];
    
    [formatter release];
    
    return result;
}

#pragma mark NSFetchedResultsController

- (NSFetchedResultsController *)resultsController
{
    if (_resultsController)
        return _resultsController;
    
    NSManagedObjectContext *context = [CoreDataManager sharedInstance].managedObjectContext;
    [context retain];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Message" inManagedObjectContext:context];
    [entity retain];
    [fetchRequest setEntity:entity];
    [entity release];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"id"
                                                                   ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    [sortDescriptor release];
    
    NSFetchedResultsController *fetchController = [[NSFetchedResultsController alloc]initWithFetchRequest:fetchRequest managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    _resultsController = fetchController;
    _resultsController.delegate = self;
    [_resultsController retain];
    
    [_resultsController performFetch:nil];
    
    [fetchController release];
    [fetchRequest release];
    [context release];
    
    return [_resultsController autorelease];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    [self.historyTableView reloadData];
}


#pragma mark TableView datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.resultsController sections] objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    id object = [self.resultsController objectAtIndexPath:indexPath];
    
    UILabel *title = (UILabel *)[cell.contentView viewWithTag:101];
    title.text = [NSString stringWithFormat:@"%@ (%@)", [object valueForKey:@"text"], [object valueForKey:@"type"]];
    
    UILabel *date = (UILabel *)[cell.contentView viewWithTag:103];
    date.text = [self convertDate:[object valueForKey:@"date"]];
    
    UILabel *status = (UILabel *)[cell.contentView viewWithTag:102];
    status.text = [object valueForKey:@"status"];
    
    return cell;
}

@end
