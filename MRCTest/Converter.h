//
//  Converter.h
//  MRCTest
//
//  Created by Nikita on 27.05.15.
//  Copyright (c) 2015 com.khodzhaiev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Converter : NSObject

+ (Converter *)sharedInstance;

- (id)convertObject:(id)object;
- (id)deserialize:(id)message;

@end
