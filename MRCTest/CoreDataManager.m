//
//  CoreDataManager.m
//  MRCTest
//
//  Created by Nikita on 27.05.15.
//  Copyright (c) 2015 com.khodzhaiev. All rights reserved.
//

#import "CoreDataManager.h"

@interface CoreDataManager ()
@property (nonatomic, retain) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@end

static CoreDataManager *_sharedInstance = nil;

@implementation CoreDataManager

+ (CoreDataManager *)sharedInstance
{
    @synchronized(self)
    {
        if (!_sharedInstance)
            _sharedInstance = [[self alloc]init];
    }
    
    return _sharedInstance;
    
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if (!_sharedInstance)
        {
            _sharedInstance = [super allocWithZone:zone];
            return _sharedInstance;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}
- (id)retain
{
    return self;
}
- (NSUInteger)retainCount
{
    return NSUIntegerMax;
}
- (oneway void)release{}

- (id)autorelease
{
    return self;
}

- (void)createObject:(NSString *)message type:(NSString *)type withId:(NSUInteger)index flag:(BOOL)secure
{
    NSManagedObject *obj = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:self.managedObjectContext];
    
    [obj setValue:message forKey:@"text"];
    [obj setValue:type forKey:@"type"];
    [obj setValue:@"Pending" forKey:@"status"];
    [obj setValue:[NSNumber numberWithInteger:index] forKey:@"id"];
    [obj setValue:[NSNumber numberWithBool:secure] forKey:@"flag"];
    
    [self save];
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInteger:index+1] forKey:@"totalNumber"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)dealloc
{
    [_managedObjectContext release];
    [_managedObjectModel release];
    [_persistentStoreCoordinator release];
    
    
    [super dealloc];
}
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel)
        return _managedObjectModel;
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"MRCTestCoreDataModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc]initWithContentsOfURL:modelURL];
    
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator)
        return _persistentStoreCoordinator;
    
    NSURL *storeURL = [[self applicationDocumentsDiretory]URLByAppendingPathComponent:@"EchoTest.sqlite"];
    [storeURL retain];
    
    NSError *error = nil;
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]initWithManagedObjectModel:self.managedObjectModel];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                   configuration:nil URL:storeURL options:nil error:&error])
    {
        NSLog(@"Unresolved error: %@, %@", error, [error userInfo]);
        [error release];
        abort();
    }
    
    [storeURL release];
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil)
        return _managedObjectContext;
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil)
    {
        _managedObjectContext = [[NSManagedObjectContext alloc]init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    
    return _managedObjectContext;
}

- (NSURL *)applicationDocumentsDiretory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)save
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil)
    {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
        {
            NSLog(@"Unresolved error: %@, %@", error, [error userInfo]);
            [error release];
            abort();
        }
    }
}


@end
