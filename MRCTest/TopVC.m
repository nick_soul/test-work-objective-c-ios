//
//  TopVC.m
//  MRCTest
//
//  Created by Nikita on 03.06.15.
//  Copyright (c) 2015 com.khodzhaiev. All rights reserved.
//

#import "TopVC.h"
#import "CoreDataManager.h"
#import <SRWebSocket.h>
#import "Converter.h"

@interface TopVC () <SRWebSocketDelegate>
@property (nonatomic, assign) IBOutlet UILabel *statusLabel;
@property (nonatomic, assign) IBOutlet UITextView *logTextView;
@property (nonatomic, assign) IBOutlet UITextField *messageTextField;
@property (nonatomic, assign) IBOutlet UISwitch *secureSwitch;
@property (nonatomic, assign) IBOutlet UISegmentedControl *segmentControl;
@property (nonatomic, assign) IBOutlet UIButton *sendButton;
@end

static NSString const *wsServer = @"ws://echo.websocket.org";

@implementation TopVC
{
    NSMutableString *logView;
    NSString *statusString;
    NSString *pendingMessage;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    logView = [[NSMutableString alloc]init];
    statusString = [[NSString alloc]init];
}

- (void)dealloc
{
    [logView release];
    [statusString release];
    [pendingMessage release];
    
    [super dealloc];
}

- (IBAction)sendMessage:(id)sender
{
    [self.messageTextField resignFirstResponder];
    
    [[CoreDataManager sharedInstance] createObject:self.messageTextField.text type:[self.segmentControl titleForSegmentAtIndex:self.segmentControl.selectedSegmentIndex] withId:[[[NSUserDefaults standardUserDefaults] valueForKey:@"totalNumber"] intValue] flag:self.secureSwitch.isOn];
    
    [self sendMessage];
}

- (void)sendMessage
{
    id object = [self getFirstPendingMessage];
    
    if (object)
    {
        [self sendPosttoServer:wsServer];
    }
}

- (void)sendPosttoServer:(NSString *)server
{
    NSURL *url = [NSURL URLWithString:server];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    SRWebSocket *rocket = [[SRWebSocket alloc]initWithURLRequest:request];
    rocket.delegate = self;
    
    [rocket open];
    [rocket autorelease];
}

// возвращаем сообщения, у которых статус Pending (если они есть)

- (id)getFirstPendingMessage
{
    NSManagedObjectContext *context = [CoreDataManager sharedInstance].managedObjectContext;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Message" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"id"
                                                                   ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    [sortDescriptor release];
    
    NSString *pending = @"Pending";
    [pending retain];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status == %@", pending];
    [fetchRequest setPredicate:predicate];
    
    [pending release];
    
    NSError *error = nil;
    NSArray *pendingMessages = [context executeFetchRequest:fetchRequest error:&error];
    
    [fetchRequest release];
    
    if (!error && [pendingMessages count] >0)
        return pendingMessages[0];
    else
    {
        return nil;
    }
}

- (NSString *)convertDate:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    NSString *result = [formatter stringFromDate:date];
    
    [formatter release];
    
    return result;
}

#pragma mark SRWebSocket

- (void)webSocketDidOpen:(SRWebSocket *)webSocket
{
    [self updateLogsWithStatus:@"Connected"];
    
    statusString = @"Status: Connected";
    
    id object = [self getFirstPendingMessage];
    
    if (object)
    {
        NSString *message = [[Converter sharedInstance] convertObject:object];
        [webSocket send:message];
        [self updateLogsWithStatus:[NSString stringWithFormat:@"Sent: %@", message]];
    }
    else
        [webSocket close];
    
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message
{
    [self updateLogsWithStatus:[NSString stringWithFormat:@"Response: %@", message]];
    
    statusString = @"Status: Received message";
    
    id object = [self getFirstPendingMessage];
    if (object)
    {
        [object setValue:@"Sent" forKey:@"status"];
        [object setValue:[NSDate date] forKey:@"date"];

        [[CoreDataManager sharedInstance] save];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Update" object:nil userInfo:@{
                                                                                               @"message" : message,
                                                                                               @"date"  : [NSDate date]}];
    
    [webSocket close];
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error
{
    statusString = @"Status: Failed with error";
    
    id object = [self getFirstPendingMessage];
    if (object)
    {
        [object setValue:@"Rejected" forKey:@"status"];
        [[CoreDataManager sharedInstance] save];
    }
    
    [self updateLogsWithStatus:[NSString stringWithFormat:@"Failed with error: %@", error]];
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean
{
    statusString = @"Status: Disconnected";
    
    [self updateLogsWithStatus:@"Disconnected"];
    
    [self sendMessage];
}

#pragma mark Update Status

- (void)updateLogsWithStatus:(NSString *)status
{
    NSDate *date = [NSDate date];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [calendar components:(NSHourCalendarUnit |
                                                         NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:date];
    
    logView = [self.logTextView.text mutableCopy];
    
    [logView appendString:status];
    [logView appendString:[NSString stringWithFormat:@" ... %d:%d:%d\n", (int)components.hour,
                           (int)components.minute, (int)components.second]];
    
    self.logTextView.text = logView;
    [logView release];
    
    [self.logTextView scrollRangeToVisible:NSMakeRange(self.logTextView.text.length, 0)];
    self.statusLabel.text = statusString;
}

@end
