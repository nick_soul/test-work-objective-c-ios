//
//  AppDelegate.h
//  MRCTest
//
//  Created by Nikita on 26.05.15.
//  Copyright (c) 2015 com.khodzhaiev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

