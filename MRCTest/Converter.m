//
//  Converter.m
//  MRCTest
//
//  Created by Nikita on 27.05.15.
//  Copyright (c) 2015 com.khodzhaiev. All rights reserved.
//

#import "Converter.h"
#import "XMLWriter.h"

@interface Converter () <NSXMLParserDelegate>

@end
static Converter *_sharedInstance = nil;

@implementation Converter
{
    NSString *convertedMessage;
}

+ (Converter *)sharedInstance
{
    @synchronized(self)
    {
        if (!_sharedInstance)
            _sharedInstance = [[self alloc]init];
    }
    
    return _sharedInstance;
    
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if (!_sharedInstance)
        {
            _sharedInstance = [super allocWithZone:zone];
            return _sharedInstance;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}
- (id)retain
{
    return self;
}
- (NSUInteger)retainCount
{
    return NSUIntegerMax;
}
- (oneway void)release{}

- (id)autorelease
{
    return self;
}

- (void)dealloc
{
    [convertedMessage release];
    
    [super dealloc];
}

- (id)convertObject:(id)object
{
    NSString *type = [object valueForKey:@"type"];
    
    if ([type isEqualToString:@"XML"])
        return [self convertXML:object];
    else if ([type isEqualToString:@"JSON"])
        return [self convertJSON:object];
    else if ([type isEqualToString:@"Binary"])
        return [self convertBinary:object];

    return nil;
}

- (id)deserialize:(id)message
{
    if ([message containsString:@"XML"])
    {
        NSData *d = [message dataUsingEncoding:NSUnicodeStringEncoding];
        
        NSXMLParser *parser = [[NSXMLParser alloc]initWithData:d];
        parser.delegate = self;
        [parser setShouldProcessNamespaces:YES];

        [parser parse];
        
        [parser autorelease];
        
        return [convertedMessage autorelease];
    }
    else if ([message containsString:@"JSON"])
    {
        NSData *data = [message dataUsingEncoding:NSASCIIStringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                             options:NSJSONReadingMutableContainers
                                                               error:nil];
        return [json valueForKey:@"text"];
    }
    else
    {
        message = [message stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSMutableArray *characters = [[NSMutableArray alloc]initWithCapacity:[message length]];
     
        for (NSUInteger i = 0; i < [message length]; i++)
        {
            NSString *ichar  = [NSString stringWithFormat:@"%c", [message characterAtIndex:i]];
            [characters addObject:ichar];
        }
        
        NSMutableString *str = [NSMutableString new];
        NSInteger counter = 0;
        
        // Splitting the array into a string of blocks of 8 charachters with a space inbetween
        for (NSString *strPart in characters)
        {
            
            [str appendString:strPart];
            counter ++;
            
            if(counter > 0 && counter%8 == 0)
                [str appendString:@" "];
        }
        
        [characters release];
        
        return [self stringFromBinString:[str autorelease]];
    }
    
    return nil;
}


// converts string to XML

- (NSString *)convertXML:(id)obj
{
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    
    [xmlWriter writeStartElement:@"Text"];
    [xmlWriter writeCharacters:[obj valueForKey:@"text"]];
    [xmlWriter writeEndElement];
    
    [xmlWriter writeStartElement:@"Flag"];
    [xmlWriter writeCharacters:[[obj valueForKey:@"flag"] stringValue]];
    [xmlWriter writeEndElement];
    
    [xmlWriter writeStartElement:@"Type"];
    [xmlWriter writeCharacters:[obj valueForKey:@"type"]];
    [xmlWriter writeEndElement];
    
    NSString *xml = [xmlWriter toString];
    [xmlWriter release];
    
    return xml;
}

// converts string to JSON

- (NSString *)convertJSON:(id)obj
{
    NSDictionary *dataDictionary = @{@"text" : [obj valueForKey:@"text"],
                                     @"flag" : [[obj valueForKey:@"flag"] stringValue],
                                     @"type" : [obj valueForKey:@"type"]
                                     
                                     };
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDictionary
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    
    NSString *jsonStr = [[NSString alloc]initWithData:jsonData encoding:NSASCIIStringEncoding];
    
    return [jsonStr autorelease];
}

- (NSString *)convertBinary:(id)obj
{
    
    return [self bitsForString:[NSString stringWithFormat:@"%@%@", [obj valueForKey:@"text"], [obj valueForKey:@"flag"]]];
}

// converts string to binary

- (NSString *)bitsForString:(NSString *)value
{
    const char *cString = [value UTF8String];
    int length = (int)strlen(cString);
    NSMutableString *result = [NSMutableString string];
    for (int i = 0; i < length; i++) {
        [result appendString:[self bits:*cString++ forSize:sizeof(char)]];
    }
    return result;
}

- (NSString *)bits:(NSInteger)value forSize:(int)size {
    const int shift = 8*size - 1;
    const unsigned mask = 1 << shift;
    NSMutableString *result = [NSMutableString string];
    for (int i = 1; i <= shift + 1; i++ ) {
        [result appendString:(value & mask ? @"1" : @"0")];
        value <<= 1;
        if (i % 8 == 0) {
            [result appendString:@" "];
        }
    }
    return result;
}

- (NSString*) stringFromBinString:(NSString *)binString
{
    NSArray *tokens = [binString componentsSeparatedByString:@" "];
    char *chars = malloc(sizeof(char) * ([tokens count] + 1));
    
    for (int i = 0; i < [tokens count]; i++) {
        const char *token_c = [[tokens objectAtIndex:i] cStringUsingEncoding:NSUTF8StringEncoding];
        char val = (char)strtol(token_c, NULL, 2);
        chars[i] = val;
    }
    chars[[tokens count]] = 0;
    NSString *result = [NSString stringWithCString:chars
                                          encoding:NSUTF8StringEncoding];
    
    free(chars);
    return result;
}


#pragma mark NSXMLParser delegate

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    convertedMessage = [[NSString alloc]initWithString:string];
}

@end
