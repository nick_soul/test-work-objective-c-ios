//
//  CoreDataManager.h
//  MRCTest
//
//  Created by Nikita on 27.05.15.
//  Copyright (c) 2015 com.khodzhaiev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NSManagedObject;
@import CoreData;
@interface CoreDataManager : NSObject

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

+ (CoreDataManager *)sharedInstance;

- (void)save;
- (void)createObject:(NSString *)message type:(NSString *)type withId:(NSUInteger)index flag:(BOOL)secure;


@end
