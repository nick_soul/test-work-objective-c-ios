//
//  RightVC.m
//  MRCTest
//
//  Created by Nikita on 03.06.15.
//  Copyright (c) 2015 com.khodzhaiev. All rights reserved.
//

#import "RightVC.h"
#import "Converter.h"

@interface RightVC ()
@property (nonatomic, assign) IBOutlet UITextView *responseTextView;

@end

@implementation RightVC
{
    NSMutableString *responseLogView;

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:@"Update" object:nil];

}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Update" object:nil];
    
    [super dealloc];
}

- (void)receivedNotification:(NSNotification *)notification
{
    if ([notification.name isEqualToString:@"Update"])
    {
        NSString *res = [[Converter sharedInstance] deserialize:notification.userInfo[@"message"]];
        
        [self updateResponseLog:res];
    }
}

- (void)updateResponseLog:(id)message
{
    responseLogView = [self.responseTextView.text mutableCopy];
    
    [responseLogView appendString:[NSString stringWithFormat:@"\n%@: ", [self convertDate:[NSDate date]]]];
    [responseLogView appendString:[NSString stringWithFormat:@"%@", message]];
    
    self.responseTextView.text = responseLogView;
    [self.responseTextView scrollRangeToVisible:NSMakeRange(self.responseTextView.text.length, 0)];
    
    [responseLogView release];
}

- (NSString *)convertDate:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    NSString *result = [formatter stringFromDate:date];
    
    [formatter release];
    
    return result;
}

@end
